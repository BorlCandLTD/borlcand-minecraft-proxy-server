BorlCand Minecraft Proxy Server
==========
The modification of the most reliable Minecraft server portal suite.
------------------------------------------------
Proxy server software which allows a user to link multiple Minecraft servers together, allowing players to teleport between them and access advanced features. This makes it perfect for servers looking to expand their player base and spread across multiple gameplay styles.

>BorlCand LTD Software