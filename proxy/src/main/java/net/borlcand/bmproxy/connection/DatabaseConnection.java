package net.borlcand.bmproxy.connection;

import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.conf.Configuration;

/**
 *
 * @author BorlCand
 */
public class DatabaseConnection
{

    @Getter
    protected final Logger logger = ProxyServer.getInstance().getLogger();

    @Getter
    protected java.sql.Connection connection;
    @Getter
    protected String dsn = "";

    public DatabaseConnection()
    {
        logger.log( Level.INFO, "[{0}] -> Starting", this.getClass().getSimpleName() );

        loadDriver( "com.mysql.jdbc.Driver" );
        loadConfiguration();
        openConnection();
    }

    public void openConnection()
    {
        try
        {
            while ( connection == null || !connection.isValid( 15 ) || connection.isClosed() )
            {
                connection = DriverManager.getConnection( this.dsn );

                logger.log( Level.INFO, "[{0}] Successfully connected to database", this.getClass().getSimpleName() );
            }
        } catch ( Exception e )
        {
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Error connecting to specified database: {0}. DSN: " + this.dsn, e.getMessage() );
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Reloading driver and config" );

            this.loadDriver( "com.mysql.jdbc.Driver" );
            this.loadConfiguration();
        }
    }

    protected void loadConfiguration()
    {
        Configuration config;
        String host, dbname, username, password;
        int port;

        config = BungeeCord.getInstance().config;

        if ( !dsn.isEmpty() )
        {
            config.load();

            logger.log( Level.INFO, "[{0}] Reloaded config", this.getClass().getSimpleName() );
        }

        host = config.getDatabaseMysqlHost();
        port = config.getDatabaseMysqlPort();
        dbname = config.getDatabaseMysqlDbname();
        username = config.getDatabaseMysqlUsername();
        password = config.getDatabaseMysqlPassword();

        dsn = String.format( "jdbc:mysql://%s:%d/%s?"
                + "user=%s&password=%s", host, port, dbname, username, password );

        logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Loaded config. DSN is: {0}", dsn );
    }

    protected void loadDriver(String driver)
    {
        try
        {
            Class.forName( driver );
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Loaded {0} driver", driver );
        } catch ( ClassNotFoundException ex )
        {
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Error while loading " + driver + " driver: {0}", ex.getMessage() );
        }
    }

}
