package net.borlcand.bmproxy.connection;

import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.BungeeCord;

/**
 *
 * @author BorlCand
 */
public abstract class DatabaseTable
{

    protected final Logger logger;
    protected final DatabaseConnection databaseConnection;
    protected final java.sql.Connection connection;
    protected final String tableName;

    public DatabaseTable(String tableName)
    {
        this( BungeeCord.getInstance().getDatabaseConnection(), tableName );
    }

    public DatabaseTable(DatabaseConnection databaseConnection, String tableName)
    {
        this.databaseConnection = databaseConnection;
        this.tableName = tableName;
        this.connection = databaseConnection.getConnection();
        this.logger = databaseConnection.getLogger();
        
        logger.log( Level.INFO, "[" + this.getClass().getSuperclass().getSimpleName() + "] -> [{0}] Enabled with Table '" + this.tableName + "'", this.getClass().getSimpleName() );
    }

    protected abstract void checkTables();

    protected abstract void addEntry(Object... params);
}
