/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.borlcand.bmproxy.metrics;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import net.borlcand.bmproxy.connection.DatabaseConnection;
import net.borlcand.bmproxy.connection.DatabaseTable;
import net.md_5.bungee.BungeeCord;

public final class AdMetrics extends DatabaseTable
{

    public AdMetrics()
    {
        super( BungeeCord.getInstance().config.getAdMetricsTableName() );

        checkTables();
    }

    public AdMetrics(DatabaseConnection dbConnection)
    {
        super( dbConnection, BungeeCord.getInstance().config.getAdMetricsTableName() );

        checkTables();
    }

    @Override
    protected void checkTables()
    {
        databaseConnection.openConnection();

        try ( PreparedStatement ps = connection.prepareStatement(
                "CREATE TABLE IF NOT EXISTS `" + tableName + "` ("
                + "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,"
                + "  `username` varchar(20) NOT NULL,"
                + "  `count` int(10) unsigned NOT NULL DEFAULT '1',"
                + "  `last_seen` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,"
                + "  `listener_host` varchar(255) DEFAULT NULL,"
                + "  `listener_port` smallint(5) unsigned DEFAULT NULL,"
                + "  PRIMARY KEY (`id`),"
                + "  UNIQUE KEY `username` (`username`)"
                + ") ENGINE=MyISAM DEFAULT CHARSET=utf8;" ) )
        {
            ps.executeUpdate();
        } catch ( SQLException ex )
        {
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Error while checking tables: {0}", ex.getMessage() );
        }
    }

    @Override
    public void addEntry(Object... params)
    {
        databaseConnection.openConnection();

        String username = (String) params[0],
                listenerHost = (String) params[1];
        int listenerPort = (int) params[2];

        String query = String.format( "INSERT INTO %s(`username`, `listener_host`, `listener_port`) VALUES ('%s', '%s', '%d') ON DUPLICATE KEY UPDATE `count`=`count`+1;", tableName, username, listenerHost, listenerPort );

        try (
                PreparedStatement ps = connection.prepareStatement(
                        query
                ) )
        {
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Adding player {0} from " + listenerHost + ":" + listenerPort, username );
            ps.executeUpdate();
        } catch ( SQLException ex )
        {
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Error while adding entry: {0}", ex.getMessage() );
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] with {0}", query );
        }
    }
}
