/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.borlcand.bmproxy.metrics;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import net.borlcand.bmproxy.connection.DatabaseConnection;
import net.borlcand.bmproxy.connection.DatabaseTable;
import net.md_5.bungee.BungeeCord;

public final class ProtocolMetrics extends DatabaseTable
{

    public ProtocolMetrics()
    {
        super( BungeeCord.getInstance().config.getProtocolMetricsTableName() );

        checkTables();
    }

    public ProtocolMetrics(DatabaseConnection dbConnection)
    {
        super( dbConnection, BungeeCord.getInstance().config.getProtocolMetricsTableName() );

        checkTables();
    }

    @Override
    protected void checkTables()
    {
        databaseConnection.openConnection();

        try ( PreparedStatement ps = connection.prepareStatement(
                "CREATE TABLE IF NOT EXISTS `" + tableName + "` ("
                + "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,"
                + "  `protocol_version` varchar(10) DEFAULT NULL,"
                + "  `listener_host` varchar(255) DEFAULT NULL,"
                + "  `listener_port` smallint(5) unsigned DEFAULT NULL,"
                + "  `time` datetime DEFAULT CURRENT_TIMESTAMP,"
                + "  PRIMARY KEY (`id`)"
                + ") ENGINE=MyISAM DEFAULT CHARSET=utf8;" ) )
        {
            ps.executeUpdate();
        } catch ( SQLException ex )
        {
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Error while checking tables: {0}", ex.getMessage() );
        }
    }

    @Override
    public void addEntry(Object... params)
    {
        databaseConnection.openConnection();

        String ip = (String) params[0],
                protocolVersion = Integer.toString( (int) params[1] ),
                listenerHost = (String) params[2];
        int listenerPort = (int) params[3];

        String query = String.format( "INSERT INTO %s(`ip`, `protocol_version`, `listener_host`, `listener_port`) VALUES ('%s', '%s', '%s', '%d');", tableName, ip, protocolVersion, listenerHost, listenerPort );

        try (
                PreparedStatement ps = connection.prepareStatement(
                        query
                ) )
        {
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Connection with protocol version {0} from " + listenerHost + ":" + listenerPort, protocolVersion );
            ps.executeUpdate();
        } catch ( SQLException ex )
        {
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Error while adding entry: {0}", ex.getMessage() );
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] with {0}", query );
        }
    }
}
