/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.borlcand.bmproxy.metrics;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import net.borlcand.bmproxy.connection.DatabaseConnection;
import net.borlcand.bmproxy.connection.DatabaseTable;
import net.md_5.bungee.BungeeCord;

public final class SessionMetrics extends DatabaseTable
{

    public SessionMetrics()
    {
        super( BungeeCord.getInstance().config.getSessionMetricsTableName() );

        checkTables();
    }

    public SessionMetrics(DatabaseConnection dbConnection)
    {
        super( dbConnection, BungeeCord.getInstance().config.getSessionMetricsTableName() );

        checkTables();
    }

    @Override
    protected void checkTables()
    {
        databaseConnection.openConnection();

        try ( PreparedStatement ps = connection.prepareStatement(
                "CREATE TABLE IF NOT EXISTS `" + tableName + "` ("
                + "  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,"
                + "  `username` varchar(20) NOT NULL,"
                + "  `login` datetime DEFAULT CURRENT_TIMESTAMP,"
                + "  `logout` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,"
                + "  `listener_host` varchar(255) DEFAULT NULL,"
                + "  `listener_port` smallint(5) unsigned DEFAULT NULL,"
                + "  `state` tinyint(3) unsigned NOT NULL DEFAULT '0',"
                + "  PRIMARY KEY (`id`)"
                + ") ENGINE=MyISAM DEFAULT CHARSET=utf8;" ) )
        {
            ps.executeUpdate();
        } catch ( SQLException ex )
        {
            logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Error while checking tables: {0}", ex.getMessage() );
        }
    }

    @Override
    public void addEntry(Object... params)
    {
        databaseConnection.openConnection();

        String username = (String) params[0],
                listenerHost = (String) params[1];
        int listenerPort = (int) params[2],
                state = (int) params[3];

        String query = "";
        if ( state == 0 )
        {
            query = String.format( "INSERT INTO %s(`username`, `listener_host`, `listener_port`) VALUES ('%s', '%s', '%d');", tableName, username, listenerHost, listenerPort );
        } else if ( state == 1 )
        {
            query = String.format( "UPDATE %s SET `state`='%d' WHERE `username`='%s' AND `state`='0' ORDER BY `id` DESC LIMIT 1;", tableName, state, username );
        }

        {
            try (
                    PreparedStatement ps = connection.prepareStatement(
                            query
                    ) )
            {
                logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Player {0} with state " + state + " from " + listenerHost + ":" + listenerPort, username );
                ps.executeUpdate();
            } catch ( SQLException ex )
            {
                logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] Error while adding entry: {0}", ex.getMessage() );
                logger.log( Level.INFO, "[" + this.getClass().getSimpleName() + "] with {0}", query );
            }
        }
    }
}
