package net.borlcand.bmproxy.command;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import net.borlcand.bmproxy.data.StaticData;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * Command to list all players and settings.
 */
public class CommandFakeOnline extends Command
{

    public CommandFakeOnline()
    {
        super( "gfo", "bmproxy.command.online" );
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        for ( ServerInfo server : ProxyServer.getInstance().getServers().values() )
        {
            if ( !server.canAccess( sender ) )
            {
                continue;
            }

            sender.sendMessage( String.format( " [%s] (%d / %d)", server.getName(), server.getPlayers().size(), server.getOnlineCount() ) );
        }

        sender.sendMessage( ProxyServer.getInstance().getTranslation( "total_players", ProxyServer.getInstance().getPlayers().size() + " * " + BungeeCord.getInstance().getOnlineMultiplier( ProxyServer.getInstance().getPlayers().size() ) + " = " + ProxyServer.getInstance().getOnlineCount() ) );
        sender.sendMessage( "Current settings:" );
        sender.sendMessage( String.format( " -> Fake online: { %s; Type: %d; Multiplier: %f }", ( BungeeCord.getInstance().config.isFakeOnline() ? "Enabled" : "Disabled" ), BungeeCord.getInstance().config.getFakeOnlineType(), BungeeCord.getInstance().getOnlineMultiplier( ProxyServer.getInstance().getPlayers().size() ) ) );
        sender.sendMessage( String.format( " -> Database: %s ", ( BungeeCord.getInstance().config.isDatabaseEnabled() ? "Enabled" : "Disabled" ) ) );
        sender.sendMessage( String.format( " -> AdMetrics: %s ", ( BungeeCord.getInstance().config.isAdMetricsEnabled() ? "Enabled" : "Disabled" ) ) );
        sender.sendMessage( String.format( " -> ProtocolMetrics: %s ", ( BungeeCord.getInstance().config.isProtocolMetricsEnabled() ? "Enabled" : "Disabled" ) ) );
    }

}
