package net.borlcand.bmproxy.command;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

public class CommandBMProxy extends Command
{

    public CommandBMProxy()
    {
        super( "bmproxy" );
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        sender.sendMessage( ChatColor.YELLOW + "This server is running " + ChatColor.BOLD + ChatColor.GOLD + ProxyServer.getInstance().getName() + ChatColor.RESET + ChatColor.YELLOW + " version " + ChatColor.BOLD + ChatColor.GOLD + ProxyServer.getInstance().getVersion() + ChatColor.AQUA + " by BorlCand LTD" );
    }
}
