package net.md_5.bungee.conf;

import com.google.common.base.Preconditions;
import gnu.trove.map.TMap;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import javax.imageio.ImageIO;
import lombok.Getter;
import net.md_5.bungee.api.Favicon;
import net.md_5.bungee.api.ProxyConfig;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.config.ConfigurationAdapter;
import net.md_5.bungee.api.config.ListenerInfo;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.util.CaseInsensitiveMap;
import net.md_5.bungee.util.CaseInsensitiveSet;

/**
 * Core configuration for the proxy.
 */
@Getter
public class Configuration implements ProxyConfig
{

    /**
     * Time before users are disconnected due to no network activity.
     */
    private int timeout = 30000;
    /**
     * UUID used for metrics.
     */
    private String uuid = UUID.randomUUID().toString();
    /**
     * Set of all listeners.
     */
    private Collection<ListenerInfo> listeners;
    /**
     * Set of all servers.
     */
    private TMap<String, ServerInfo> servers;
    /**
     * Should we check minecraft.net auth.
     */
    private boolean onlineMode = false;
    private int playerLimit = -1;
    private Collection<String> disabledCommands;
    private int throttle = 4000;
    private boolean ipForward;
    private Favicon favicon;
    private boolean fakeOnline = false;
    /**
     * Type of fake online.
     *
     * @serial 0 - automatic(dynamic) multiplier
     * @serial 1 - fixed multiplier
     */
    private int fakeOnlineType = 0;
    private double fakeOnlineMultiplier = 1;
    private boolean adMetricsEnabled = false;
    private String adMetricsTableName = "admetrics";
    private boolean protocolMetricsEnabled = false;
    private String protocolMetricsTableName = "protocolmetrics";
    private boolean sessionMetricsEnabled = false;
    private String sessionMetricsTableName = "sessionmetrics";
    private boolean databaseEnabled = false;
    private String databaseType = "mysql";
    private String databaseMysqlHost = "localhost";
    private int databaseMysqlPort = 3306;
    private String databaseMysqlDbname = "dbname";
    private String databaseMysqlUsername = "username";
    private String databaseMysqlPassword = "password";

    public void load()
    {
        ConfigurationAdapter adapter = ProxyServer.getInstance().getConfigurationAdapter();
        adapter.load();

        File fav = new File( "server-icon.png" );
        if ( fav.exists() )
        {
            try
            {
                favicon = Favicon.create( ImageIO.read( fav ) );
            } catch ( IOException | IllegalArgumentException ex )
            {
                ProxyServer.getInstance().getLogger().log( Level.WARNING, "Could not load server icon", ex );
            }
        }

        listeners = adapter.getListeners();
        timeout = adapter.getInt( "timeout", timeout );
        uuid = adapter.getString( "stats", uuid );
        onlineMode = adapter.getBoolean( "online_mode", onlineMode );
        playerLimit = adapter.getInt( "player_limit", playerLimit );
        throttle = adapter.getInt( "connection_throttle", throttle );
        ipForward = adapter.getBoolean( "ip_forward", ipForward );
        fakeOnline = adapter.getBoolean( "bmproxy.fakeonline.enabled", fakeOnline );
        fakeOnlineType = adapter.getInt( "bmproxy.fakeonline.type", fakeOnlineType );
        fakeOnlineMultiplier = (double) adapter.getInt( "bmproxy.fakeonline.multiplier", (int) ( fakeOnlineMultiplier * 100 ) ) / 100;
        adMetricsEnabled = adapter.getBoolean( "bmproxy.admetrics.enabled", adMetricsEnabled );
        adMetricsTableName = adapter.getString( "bmproxy.admetrics.tablename", adMetricsTableName );
        protocolMetricsEnabled = adapter.getBoolean( "bmproxy.protocolmetrics.enabled", protocolMetricsEnabled );
        protocolMetricsTableName = adapter.getString( "bmproxy.protocolmetrics.tablename", protocolMetricsTableName );
        sessionMetricsEnabled = adapter.getBoolean( "bmproxy.sessionmetrics.enabled", sessionMetricsEnabled );
        sessionMetricsTableName = adapter.getString( "bmproxy.sessionmetrics.tablename", sessionMetricsTableName );
        databaseEnabled = adapter.getBoolean( "bmproxy.database.enabled", databaseEnabled );
        databaseType = adapter.getString( "bmproxy.database.type", databaseType );
        if ( databaseType.equalsIgnoreCase( "mysql" ) )
        {
            databaseMysqlHost = adapter.getString( "bmproxy.database.mysql.host", databaseMysqlHost );
            databaseMysqlPort = adapter.getInt( "bmproxy.database.mysql.port", databaseMysqlPort );
            databaseMysqlDbname = adapter.getString( "bmproxy.database.mysql.dbname", databaseMysqlDbname );
            databaseMysqlUsername = adapter.getString( "bmproxy.database.mysql.username", databaseMysqlUsername );
            databaseMysqlPassword = adapter.getString( "bmproxy.database.mysql.password", databaseMysqlPassword );
        }
        disabledCommands = new CaseInsensitiveSet( (Collection<String>) adapter.getList( "disabled_commands", Arrays.asList( "disabledcommandhere" ) ) );

        Preconditions.checkArgument( listeners != null && !listeners.isEmpty(), "No listeners defined." );

        Map<String, ServerInfo> newServers = adapter.getServers();
        Preconditions.checkArgument( newServers != null && !newServers.isEmpty(), "No servers defined" );

        if ( servers == null )
        {
            servers = new CaseInsensitiveMap<>( newServers );
        } else
        {
            for ( ServerInfo oldServer : servers.values() )
            {
                // Don't allow servers to be removed
                Preconditions.checkArgument( newServers.containsKey( oldServer.getName() ), "Server %s removed on reload!", oldServer.getName() );
            }

            // Add new servers
            for ( Map.Entry<String, ServerInfo> newServer : newServers.entrySet() )
            {
                if ( !servers.containsValue( newServer.getValue() ) )
                {
                    servers.put( newServer.getKey(), newServer.getValue() );
                }
            }
        }

        for ( ListenerInfo listener : listeners )
        {
            Preconditions.checkArgument( servers.containsKey( listener.getDefaultServer() ), "Default server %s is not defined", listener.getDefaultServer() );
            Preconditions.checkArgument( servers.containsKey( listener.getFallbackServer() ), "Fallback server %s is not defined", listener.getFallbackServer() );
            for ( String server : listener.getForcedHosts().values() )
            {
                if ( !servers.containsKey( server ) )
                {
                    ProxyServer.getInstance().getLogger().log( Level.WARNING, "Forced host server {0} is not defined", server );
                }
            }
        }
    }

    @Override
    @Deprecated
    public String getFavicon()
    {
        return getFaviconObject().getEncoded();
    }

    @Override
    public Favicon getFaviconObject()
    {
        return favicon;
    }
}
